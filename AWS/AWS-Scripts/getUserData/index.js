/** lambda aws -  to get sample code*/

/** "index.handler" calls exports.handler in index.js 
 * -- handler is a function imported from aws lambda function
 * -- If handler function is invoked by rest service then 
 * 1. event : event triggered by lambda - consists of request body,url paramaters
 * 2. context : provides method information 
 * 3. callback : used to return to function */


'use strict'
const AWS = require('aws-sdk');

//To setup region
Aws.config.update({region: "ap-south-1"})
exports.handler = function (event, context, callback) {
    console.log(JSON.stringify(`Event: event`))

}